#Inuit Project

This is an opinionated way to start a project with [InuitCSS](https://github.com/inuitcss)
Like the [inuit-starter-kit](https://github.com/inuitcss/starter-kit) takes some inuitcss files
to be imported to your CSS project.


Please see the `main.scss` file for more information about how to launch this project.